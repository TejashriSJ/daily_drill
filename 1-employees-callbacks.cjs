/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const fs = require("fs");

function retriveDataForIds(idsArray, data, retriveDataCallBack) {
  try {
    let dataForIds = data[Object.keys(data)].filter((employee) => {
      return idsArray.includes(employee.id);
    });
    retriveDataCallBack("", dataForIds);
  } catch (err) {
    retriveDataCallBack(err);
  }
}

function groupDataBasedOnCompanies(data, groupDataCallBack) {
  try {
    let groupedData = data[Object.keys(data)].reduce((groupedObj, employee) => {
      groupedObj[employee.company] ?? (groupedObj[employee.company] = []);
      groupedObj[employee.company].push({ ...employee });
      return groupedObj;
    }, {});
    groupDataCallBack("", groupedData);
  } catch (err) {
    groupDataCallBack(err);
  }
}

function getDataForPowerpuffBrigade(
  groupedData,
  companyName,
  companyDataCallback
) {
  try {
    let givenCompanyData = Object.entries(groupedData).filter(
      ([name, details]) => {
        return name === companyName;
      }
    );
    companyDataCallback("", givenCompanyData.flat(2));
  } catch (err) {
    companyDataCallback(err);
  }
}

function removeID(data, id, removeCallback) {
  try {
    let dataAfterRemovedId = data[Object.keys(data)].filter((employee) => {
      return employee.id !== id;
    });
    removeCallback("", dataAfterRemovedId);
  } catch (err) {
    removeCallback(err);
  }
}

function sortDataBasedOnCompany(data, sortDataCallback) {
  try {
    let sortedData = data[Object.keys(data)].sort((employee1, employee2) => {
      if (employee1.company.localeCompare(employee2.company) === 0) {
        return employee1.id - employee2.id;
      } else {
        return employee1.company.localeCompare(employee2.company);
      }
    });
    sortDataCallback("", sortedData);
  } catch (err) {
    sortDataCallback(err);
  }
}

function swapPositions(data, id1, id2, swapCallback) {
  try {
    let idsData = data[Object.keys(data)].filter((employee) => {
      return employee.id === id1 || employee.id === id2;
    });
    let swapedData = data[Object.keys(data)].reduce(
      (afterSwapedObj, employee) => {
        if (employee.id === id1) {
          let newEmployeeObj = { ...employee };
          newEmployeeObj.company = idsData[1].company;
          afterSwapedObj.push(newEmployeeObj);
        } else if (employee.id === id2) {
          let newEmployeeObj = { ...employee };
          newEmployeeObj.company = idsData[0].company;
          afterSwapedObj.push(newEmployeeObj);
        } else {
          afterSwapedObj.push(employee);
        }
        return afterSwapedObj;
      },
      []
    );
    swapCallback("", swapedData);
  } catch (err) {
    swapCallback(err);
  }
}

function addBirthday(data, addBirthdayCallback) {
  try {
    let addedBirthdayData = data[Object.keys(data)].map((employee) => {
      if (employee.id % 2 === 0) {
        let newObj = { ...employee };

        newObj["Birthday"] = new Date().toDateString();
        return newObj;
      } else {
        return employee;
      }
    });
    addBirthdayCallback("", addedBirthdayData);
  } catch (err) {
    addBirthdayCallback(err);
  }
}

const callbacksData = function () {
  // read data from json file
  fs.readFile("data.json", "utf8", (err, data) => {
    if (err) {
      console.error("Error in reading data file", err);
    } else {
      data = JSON.parse(data);

      //1. Retrieve data for ids : [2, 13, 23].
      let idsArray = [2, 13, 23];
      retriveDataForIds(idsArray, data, (err, dataForIds) => {
        if (err) {
          console.error("Error in retriving data", err);
        } else {
          fs.writeFile("dataForIds.json", JSON.stringify(dataForIds), (err) => {
            if (err) {
              console.error("Error in writing to file dataForIds", err);
            } else {
              console.log(
                "Retrieving data for ids : [2, 13, 23] completed successfully"
              );
              //2. Group data based on companies.
              groupDataBasedOnCompanies(data, (err, groupedData) => {
                if (err) {
                  console.error("Error in Grouping data", err);
                } else {
                  fs.writeFile(
                    "groupedData.json",
                    JSON.stringify(groupedData),
                    (err) => {
                      if (err) {
                        console.error(
                          "Error in writing to file groupedData",
                          err
                        );
                      } else {
                        console.log(
                          "Grouping data based on companies completed successfully"
                        );
                        //3. Get all data for company Powerpuff Brigade
                        let companyName = "Powerpuff Brigade";
                        getDataForPowerpuffBrigade(
                          groupedData,
                          companyName,
                          (err, dataForCompany) => {
                            if (err) {
                              console.error(
                                "Error in getting Powerpuff Brigade company data",
                                err
                              );
                            } else {
                              fs.writeFile(
                                "powerpuffBrigadeData.json",
                                JSON.stringify(dataForCompany),
                                (err) => {
                                  if (err) {
                                    console.error(
                                      "Error in writing to file powerpuffBrigadeData",
                                      err
                                    );
                                  } else {
                                    console.log(
                                      "Getting all data for company Powerpuff Brigade completed successfully"
                                    );
                                    //4. Remove entry with id 2.
                                    let id = 2;
                                    removeID(
                                      data,
                                      id,
                                      (err, dataAfterRemovedId) => {
                                        if (err) {
                                          console.error(
                                            `Error in removing data of ${id}`
                                          );
                                        } else {
                                          fs.writeFile(
                                            "dataAfterRemovedID.json",
                                            JSON.stringify(dataAfterRemovedId),
                                            (err) => {
                                              if (err) {
                                                console.error(
                                                  "Error in writing to file dataAfterRemovedID",
                                                  err
                                                );
                                              } else {
                                                console.log(
                                                  "Removing entry with id 2 completed successfully"
                                                );
                                                //5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
                                                sortDataBasedOnCompany(
                                                  data,
                                                  (err, sortedData) => {
                                                    if (err) {
                                                      console.error(
                                                        "Error in sorting data",
                                                        err
                                                      );
                                                    } else {
                                                      fs.writeFile(
                                                        "sortedDataBasedOnCompany.json",
                                                        JSON.stringify(
                                                          sortedData
                                                        ),
                                                        (err) => {
                                                          if (err) {
                                                            console.error(
                                                              "Error in writing to file sortedDataBasedOnCompany",
                                                              err
                                                            );
                                                          } else {
                                                            console.log(
                                                              "Sorting data based on company name completed successfully"
                                                            );
                                                            // 6. Swap position of companies with id 93 and id 92.
                                                            let id1 = 93;
                                                            let id2 = 92;
                                                            swapPositions(
                                                              data,
                                                              id1,
                                                              id2,
                                                              (
                                                                err,
                                                                dataAfterSwaped
                                                              ) => {
                                                                if (err) {
                                                                  console.error(
                                                                    "Error in swaping data",
                                                                    err
                                                                  );
                                                                } else {
                                                                  fs.writeFile(
                                                                    "swapedData.json",
                                                                    JSON.stringify(
                                                                      dataAfterSwaped
                                                                    ),
                                                                    (err) => {
                                                                      if (err) {
                                                                        console.error(
                                                                          "Error in writing to file swapedData",
                                                                          err
                                                                        );
                                                                      } else {
                                                                        console.log(
                                                                          "Swaping position of companies with id 93 and id 92 completed successfully"
                                                                        );
                                                                        // 7. For every employee whose id is even, add the birthday to their information.

                                                                        addBirthday(
                                                                          data,
                                                                          (
                                                                            err,
                                                                            birthdayAddedData
                                                                          ) => {
                                                                            if (
                                                                              err
                                                                            ) {
                                                                              console.log(
                                                                                "Error in adding birthday",
                                                                                err
                                                                              );
                                                                            } else {
                                                                              fs.writeFile(
                                                                                "addedBirthday.json",
                                                                                JSON.stringify(
                                                                                  birthdayAddedData
                                                                                ),
                                                                                (
                                                                                  err
                                                                                ) => {
                                                                                  if (
                                                                                    err
                                                                                  ) {
                                                                                    console.error(
                                                                                      "Error in writing to file addedBirthday",
                                                                                      err
                                                                                    );
                                                                                  } else {
                                                                                    console.log(
                                                                                      "All operations completed successfully"
                                                                                    );
                                                                                  }
                                                                                }
                                                                              );
                                                                            }
                                                                          }
                                                                        );
                                                                      }
                                                                    }
                                                                  );
                                                                }
                                                              }
                                                            );
                                                          }
                                                        }
                                                      );
                                                    }
                                                  }
                                                );
                                              }
                                            }
                                          );
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    }
                  );
                }
              });
            }
          });
        }
      });
    }
  });
};

callbacksData();
