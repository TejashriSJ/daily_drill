/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/
//const lipsum = require("node-lipsum");
const fs = require("fs");
const path = require("path");
const logFile = path.join(__dirname, "logfile.log");

function readFile(fileName) {
  return new Promise((resolve, reject) => {
    fs.readFile(fileName, "utf-8", (err, data) => {
      if (err) {
        console.error(`Error in reading file \n ${fileName} \n`);
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

function createFile(fileName, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(fileName, data, (err) => {
      if (err) {
        console.error(`Error in creating file \n ${fileName}`);
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function deleteFile(fileName) {
  return new Promise((resolve, reject) => {
    fs.unlink(fileName, (err) => {
      if (err) {
        console.error(`Error in deleting file \n ${fileName}`);
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

//Q1

function createAndDeleteFile() {
  // Create Array of file names
  let fileCount = 2;
  let filesGenaratingArray = new Array(fileCount).fill(0);
  let fileNames = filesGenaratingArray.map((element, index) => {
    return index + ".txt";
  });

  // Create each file in the array
  let createdFilePromises = fileNames.map((file) => {
    let filePath = path.join(__dirname, file);
    let fileContent = "Random";
    return createFile(filePath, fileContent);
  });
  Promise.all(createdFilePromises)
    .then(() => {
      console.log("All files created successfully");
    })
    .catch((err) => {
      console.error("All files did not created Successfully", err);
    });

  // Calling delete function after 2 seconds
  setTimeout(() => {
    let deletedFilesPromises = fileNames.map((file) => {
      let filePath = path.join(__dirname, file);
      return deleteFile(filePath);
    });
    Promise.all(deletedFilesPromises)
      .then(() => {
        console.log("All files deleted successfully");
      })
      .catch((err) => {
        console.error("All files did not deleted successfully", err);
      });
  }, 2000);
}
createAndDeleteFile();
// Q2

function lipsumCreateAndDelete() {
  let originalLIpsumFile = path.join(__dirname, "originalLipsum.txt");
  let newLipsumFile = path.join(__dirname, "newLipsum.txt");
  createFile(
    originalLIpsumFile,
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis elit nisl. Aliquam congue ut orci quis gravida. Etiam eleifend ultrices turpis, ut gravida ligula accumsan vel. Cras sagittis ex ac rutrum congue. Curabitur tristique nibh posuere nisi varius, non ullamcorper sapien venenatis. Maecenas non suscipit ex. Etiam commodo magna ullamcorper ex dignissim, sit amet lacinia dolor molestie. Morbi vitae scelerisque magna. Phasellus pulvinar, mauris id ornare consequat, elit sem fermentum nisl, eget varius sapien risus at felis.\n\nCurabitur eu accumsan turpis. Fusce finibus sem euismod massa fringilla, at maximus sem pulvinar. Proin sollicitudin porta leo sed pellentesque. Mauris nisi dolor, posuere in neque sit amet, sodales lobortis justo. Etiam venenatis metus aliquet purus porta, a aliquam nunc ullamcorper. Maecenas dapibus nibh non nunc facilisis, sit amet consequat lectus scelerisque. Pellentesque neque elit, sodales a commodo vel, vulputate in diam. Quisque quis lacus hendrerit, viverra dolor vel, volutpat nisi. Sed facilisis sollicitudin lorem, sit amet vulputate ipsum convallis sit amet. Praesent ipsum orci, vestibulum non consectetur id, molestie sit amet sem. Phasellus interdum purus id erat tempor rhoncus. In viverra quam et neque placerat malesuada. Donec interdum nulla non ante finibus, eget posuere sem ultricies.\n\nVestibulum tempor aliquam ipsum sit amet pretium. Quisque egestas risus nisi, vel pharetra arcu ultricies a. Quisque ornare nunc sit amet libero pharetra consectetur. Aenean ligula dui, faucibus vitae tempor in, molestie ut nibh. Donec finibus ex ex, et aliquet urna accumsan et. Duis felis nisi, tincidunt nec magna sed, tincidunt volutpat lectus. Mauris at gravida ipsum. Duis vel velit lectus. Etiam auctor fermentum tortor, eget condimentum elit"
  )
    .then(() => {
      return readFile(originalLIpsumFile);
    })
    .then((data) => {
      console.log(`Lipsum file read successfully`);
      return createFile(newLipsumFile, data);
    })
    .then(() => {
      console.log(`Writed to new File Successfully`);
      return deleteFile(originalLIpsumFile);
    })
    .then(() => {
      console.log(`Original file deleted`);
    })
    .catch((err) => {
      console.error(`Error in doing file operations`, err);
    });
}

lipsumCreateAndDelete();

//Q3.
/*
B. Write logic to logData after each activity for each user. Following are the list of activities
"Login Success"
"Login Failure"
"GetData Success"
"GetData Failure"

Call log data function after each activity to log that activity in the file.
All logged activity must also include the timestamp for that activity.

You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
All calls must be chained unless mentioned otherwise.
*/
function login(user, val) {
  if (val % 2 === 0) {
    return Promise.resolve(user);
  } else {
    return Promise.reject(new Error("User not found"));
  }
}

function getData() {
  return Promise.resolve([
    {
      id: 1,
      name: "Test",
    },
    {
      id: 2,
      name: "Test 2",
    },
  ]);
}

function logData(user, activity) {
  fileData = JSON.stringify({
    user: user,
    activity: activity,
    timestamp: new Date(),
  });
  return new Promise((resolve, reject) => {
    fs.appendFile(logFile, fileData, (err) => {
      if (err) {
        console.error(`Error in writing to file \n ${pathToFile}`);
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

//A. login with value 3 and call getData once login is successful

function loginAndGetData(user, val) {
  login(user, val)
    .then((user) => {
      return getData();
    })
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.error(err);
    });
}
loginAndGetData("Tejashri", 3);

// B.Write logic to logData after each activity for each user

function logDataForEachActivity(user, val) {
  login(user, val)
    .then((user) => {
      return logData(user, "Login Success");
    })
    .then(() => {
      return getData();
    })
    .then(() => {
      return logData(user, "GetData Success");
    })
    .then(() => {
      console.log("Data loged for the user after successful login");
    })
    .catch((err) => {
      if (err.message === "User not found") {
        logData(user, "Login Failure")
          .then(() => {
            return logData(user, "GetData Failure");
          })
          .then(() => {
            console.log("Data loged for the user after login failed");
          })
          .catch((err) => {
            console.error("Error in loging data", err);
          });
      } else {
        console.error(err);
      }
    });
}
let user = "Tejashri";
let val = 2;
logDataForEachActivity(user, val);
