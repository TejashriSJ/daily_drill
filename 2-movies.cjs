const favouritesMovies = {
  Matrix: {
    imdbRating: 8.3,
    actors: ["Keanu Reeves", "Carrie-Anniee"],
    oscarNominations: 2,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$680M",
  },
  FightClub: {
    imdbRating: 8.8,
    actors: ["Edward Norton", "Brad Pitt"],
    oscarNominations: 6,
    genre: ["thriller", "drama"],
    totalEarnings: "$350M",
  },
  Inception: {
    imdbRating: 8.3,
    actors: ["Tom Hardy", "Leonardo Dicaprio"],
    oscarNominations: 12,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$870M",
  },
  "The Dark Knight": {
    imdbRating: 8.9,
    actors: ["Christian Bale", "Heath Ledger"],
    oscarNominations: 12,
    genre: ["thriller"],
    totalEarnings: "$744M",
  },
  "Pulp Fiction": {
    imdbRating: 8.3,
    actors: ["Sameul L. Jackson", "Bruce Willis"],
    oscarNominations: 7,
    genre: ["drama", "crime"],
    totalEarnings: "$455M",
  },
  Titanic: {
    imdbRating: 8.3,
    actors: ["Leonardo Dicaprio", "Kate Winslet"],
    oscarNominations: 13,
    genre: ["drama"],
    totalEarnings: "$800M",
  },
};

/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/

//  Q1. Find all the movies with total earnings more than $500M.

const totalEarningMoreThan500M = function () {
  return Object.keys(favouritesMovies)
    .filter((movie) => {
      return (
        parseInt(favouritesMovies[movie].totalEarnings.replace("$", "")) > 500
      );
    })
    .reduce((movieObj, eachMovie) => {
      movieObj[eachMovie] = { ...favouritesMovies[eachMovie] };
      return movieObj;
    }, {});
};

console.log(
  "Movies with Total earning more than $500M are: \n",
  totalEarningMoreThan500M()
);

// Q2. Find all the movies who got more than 3 oscarNominations
// and also totalEarning are more than $500M.

const movieWithGoodOscarAndEarnig = function () {
  let moviesWithGreaterEarning = totalEarningMoreThan500M();
  return Object.keys(moviesWithGreaterEarning)
    .filter((movie) => {
      return moviesWithGreaterEarning[movie].oscarNominations > 3;
    })
    .reduce((newMovieObj, eachMovie) => {
      newMovieObj[eachMovie] = { ...moviesWithGreaterEarning[eachMovie] };
      return newMovieObj;
    }, {});
};

console.log(
  "Movies with more than 3 oscar Nominations and totalEarning more than $500M: \n",
  movieWithGoodOscarAndEarnig()
);

// Q.3 Find all movies of the actor "Leonardo Dicaprio".

const moviesOfLeonardoDicaprio = function () {
  return Object.keys(favouritesMovies)
    .filter((movie) => {
      return favouritesMovies[movie].actors.includes("Leonardo Dicaprio");
    })
    .reduce((newMovieObj, eachMovie) => {
      newMovieObj[eachMovie] = { ...favouritesMovies[eachMovie] };
      return newMovieObj;
    }, {});
};
console.log("Movies of Leonardo Dicaprio are: \n", moviesOfLeonardoDicaprio());

//Q.4 Sort movies (based on IMDB rating)
//if IMDB ratings are same, compare totalEarning as the secondary metric.

const sortMovies = function () {
  return Object.keys(favouritesMovies)
    .sort((movie1, movie2) => {
      if (
        favouritesMovies[movie1].imdbRating ===
        favouritesMovies[movie2].imdbRating
      ) {
        let movie1TotalEarning = parseInt(
          favouritesMovies[movie1].totalEarnings.replace("$", "")
        );
        let movie2TotalEarning = parseInt(
          favouritesMovies[movie2].totalEarnings.replace("$", "")
        );
        return movie2TotalEarning - movie1TotalEarning;
      } else {
        return (
          favouritesMovies[movie2].imdbRating -
          favouritesMovies[movie1].imdbRating
        );
      }
    })
    .reduce((newMovieObj, eachMovie) => {
      newMovieObj[eachMovie] = { ...favouritesMovies[eachMovie] };
      return newMovieObj;
    }, {});
};
console.log(
  "Movies sorted based on IMDB rating and total salary : \n",
  sortMovies()
);

//Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//drama > sci-fi > adventure > thriller > crime

const groupBasedOnGenre = function () {
  return Object.keys(favouritesMovies).reduce(
    (groupedObj, movie) => {
      let movieObj = {};
      movieObj[movie] = { ...favouritesMovies[movie] };

      if (favouritesMovies[movie].genre.includes("drama")) {
        groupedObj["drama"].push(movieObj);
      } else if (favouritesMovies[movie].genre.includes("sci-fi")) {
        groupedObj["sci-fi"].push(movieObj);
      } else if (favouritesMovies[movie].genre.includes("adventure")) {
        groupedObj["adventure"].push(movieObj);
      } else if (favouritesMovies[movie].genre.includes("thriller")) {
        groupedObj["thriller"].push(movieObj);
      } else if (favouritesMovies[movie].genre.includes("crime")) {
        groupedObj["crime"].push(movieObj);
      }
      return groupedObj;
    },
    { drama: [], "sci-fi": [], adventure: [], thriller: [], crime: [] }
  );
};
console.log("Grouped movies based on genre: \n ", groupBasedOnGenre());
