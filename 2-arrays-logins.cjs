let people = [
  {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  },
  {
    id: 2,
    first_name: "Kenneth",
    last_name: "Hinemoor",
    email: "khinemoor1@yellowbook.com",
    gender: "Polygender",
    ip_address: "50.231.58.150",
  },
  {
    id: 3,
    first_name: "Roman",
    last_name: "Sedcole",
    email: "rsedcole2@addtoany.com",
    gender: "Genderqueer",
    ip_address: "236.52.184.83",
  },
  {
    id: 4,
    first_name: "Lind",
    last_name: "Ladyman",
    email: "lladyman3@wordpress.org",
    gender: "Male",
    ip_address: "118.12.213.144",
  },
  {
    id: 5,
    first_name: "Jocelyne",
    last_name: "Casse",
    email: "jcasse4@ehow.com",
    gender: "Agender",
    ip_address: "176.202.254.113",
  },
  {
    id: 6,
    first_name: "Stafford",
    last_name: "Dandy",
    email: "sdandy5@exblog.jp",
    gender: "Female",
    ip_address: "111.139.161.143",
  },
  {
    id: 7,
    first_name: "Jeramey",
    last_name: "Sweetsur",
    email: "jsweetsur6@youtube.com",
    gender: "Genderqueer",
    ip_address: "196.247.246.106",
  },
  {
    id: 8,
    first_name: "Anna-diane",
    last_name: "Wingar",
    email: "awingar7@auda.org.au",
    gender: "Agender",
    ip_address: "148.229.65.98",
  },
  {
    id: 9,
    first_name: "Cherianne",
    last_name: "Rantoul",
    email: "crantoul8@craigslist.org",
    gender: "Genderfluid",
    ip_address: "141.40.134.234",
  },
  {
    id: 10,
    first_name: "Nico",
    last_name: "Dunstall",
    email: "ndunstall9@technorati.com",
    gender: "Female",
    ip_address: "37.12.213.144",
  },
];

/* 

    1. Find all people who are Agender
    2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    3. Find the sum of all the second components of the ip addresses.
    3. Find the sum of all the fourth components of the ip addresses.
    4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    5. Filter out all the .org emails
    6. Calculate how many .org, .au, .com emails are there
    7. Sort the data in descending order of first name

    NOTE: Do not change the name of this file

*/
// 1. Find all people who are Agender
function Agender() {
  let AgenderPerson = people.filter((person) => {
    return person["gender"] === "Agender";
  });
  return JSON.stringify(AgenderPerson);
}
console.log("Agender: " + "\n" + Agender());

//2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
function IPaddressComponent() {
  let IPComponent = [];
  people.forEach((person) => {
    person.ip_address = person.ip_address.split(".").map((ip) => {
      return Number(ip);
    });
  });
  return JSON.stringify(people);
}
console.log("\n IPaddressComponent: \n " + IPaddressComponent());

//3. Find the sum of all the second components of the ip addresses.

function sumOfSecondComponent() {
  let sumOfSecondIP = people.reduce((sum, person) => {
    return (sum += person.ip_address[1]);
  }, 0);
  return sumOfSecondIP;
}
console.log("\n Sum of Second component of IPs: \n" + sumOfSecondComponent());

//3. Find the sum of all the fourth components of the ip addresses.
function sumOfFourthComponent() {
  let sumOfFourthIP = people.reduce((sum, person) => {
    return (sum += person.ip_address[3]);
  }, 0);
  return sumOfFourthIP;
}
console.log("\n Sum of fourth component of IPs: \n" + sumOfFourthComponent());

//4. Compute the full name of each person and store it in a new key (full_name or something) for each person.

function fullName() {
  people.forEach((person) => {
    person["full_name"] = person.first_name + " " + person.last_name;
  });
  return JSON.stringify(people);
}
console.log("\n Full names: \n" + fullName());

//5. Filter out all the .org emails
function orgMails() {
  let orgMailsArray = people.filter((person) => {
    return person.email.endsWith(".org");
  });
  return JSON.stringify(orgMailsArray);
}
console.log("\n .org Mails: \n" + orgMails());

//6. Calculate how many .org, .au, .com emails are there

function differntEmailCount() {
  let emailCountObj = { ".org": 0, ".au": 0, ".com": 0 };
  people.forEach((person) => {
    if (person.email.endsWith(".org")) {
      emailCountObj[".org"] += 1;
    } else if (person.email.endsWith(".au")) {
      emailCountObj[".au"] += 1;
    } else if (person.email.endsWith(".com")) {
      emailCountObj[".com"] += 1;
    }
  });
  return JSON.stringify(emailCountObj);
}
console.log("\n email count: \n" + differntEmailCount());

//7. Sort the data in descending order of first name

function decendingOrderNames() {
  people.sort((person1, person2) => {
    return person2.first_name
      .toLocaleLowerCase()
      .localeCompare(person1.first_name.toLocaleLowerCase());
  });
  return JSON.stringify(people);
}
console.log(
  "\n Data in decending order of first name \n" + decendingOrderNames()
);
