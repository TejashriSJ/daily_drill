const items = [
  {
    name: "Orange",
    available: true,
    contains: "Vitamin C",
  },
  {
    name: "Mango",
    available: true,
    contains: "Vitamin K, Vitamin C",
  },
  {
    name: "Pineapple",
    available: true,
    contains: "Vitamin A",
  },
  {
    name: "Raspberry",
    available: false,
    contains: "Vitamin B, Vitamin A",
  },
  {
    name: "Grapes",
    contains: "Vitamin D",
    available: false,
  },
];

/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/

// 1. Get all items that are available

function getAllItems() {
  const itemNames = items.filter((item) => {
    return item.available;
  });
  return JSON.stringify(itemNames);
}
console.log("Items Available \n", getAllItems());

//2. Get all items containing only Vitamin C.

function vitaminCitems() {
  const itemsWithVitaminC = items.filter((item) => {
    return item.contains.includes("Vitamin C");
  });
  return JSON.stringify(itemsWithVitaminC);
}
console.log("Items with vitamin C : \n", vitaminCitems());

//3. Get all items containing Vitamin A.

function vitaminAitems() {
  const itemsWithVitaminA = items.filter((item) => {
    return item.contains.includes("Vitamin A");
  });
  return JSON.stringify(itemsWithVitaminA);
}
console.log("Items with vitamin A : \n ", vitaminAitems());

//  4. Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }

// and so on for all items and all Vitamins.
function groupByVitamins() {
  let newItems = items.reduce((acc, vitamins) => {
    vitamins.contains = vitamins.contains.split(",");
    let vitaminsName = vitamins.contains.map((vitamin) => {
      return vitamin.trim();
    });
    vitaminsName.map((vitamin) => {
      if (acc[vitamin] === undefined) {
        acc[vitamin] = [];
      }
      acc[vitamin].push(vitamins.name);
    });
    return acc;
  }, {});
  return JSON.stringify(newItems);
}
console.log("Grouped Items : \n ", groupByVitamins());

// 5. Sort items based on number of Vitamins they contain.
function sortItems() {
  const sortedItems = items.sort((item1, item2) => {
    return item1.contains.length - item2.contains.length;
  });
  return JSON.stringify(sortedItems);
}
console.log("Sorted Items : \n", sortItems());

module.exports.item = items;
