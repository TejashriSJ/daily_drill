const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interests: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

//Q1 Find all users who are interested in playing video games.

const videoGamers = function () {
  let videoGamersObj = Object.keys(users)
    .filter((user) => {
      return users[user].interests[0].includes("Video Games");
    })
    .reduce((newUserObj, user) => {
      newUserObj[user] = { ...users[user] };
      return newUserObj;
    }, {});
  return videoGamersObj;
};
console.log("Users intrested in playing video games: \n", videoGamers());

//Q2 Find all users staying in Germany.

const GermanyPeople = function () {
  let usersInGermany = Object.keys(users)
    .filter((user) => {
      return users[user].nationality === "Germany";
    })
    .reduce((newUserObj, user) => {
      newUserObj[user] = { ...users[user] };
      return newUserObj;
    }, {});
  return usersInGermany;
};
console.log("People in Germany are : \n", GermanyPeople());

//Q3 Sort users based on their seniority level
// for Designation - Senior Developer > Developer > Intern
// for Age - 20 > 10

const sortUsers = function () {
  let sortedData = Object.entries(users).sort(
    ([user1, object1], [user2, object2]) => {
      if (object1.desgination.includes("Senior")) {
        if (!object2.desgination.includes("Senior")) {
          return -1;
        } else {
          return object2.age - object1.age;
        }
      } else if (object1.desgination.includes("Developer")) {
        if (object2.desgination.includes("Senior")) {
          return 1;
        } else if (object2.desgination.includes("Developer")) {
          return object2.age - object1.age;
        } else {
          return -1;
        }
      } else {
        if (!object2.desgination.includes("Intern")) {
          return 1;
        } else {
          return object2.age - object1.age;
        }
      }
    }
  );
  return sortedData;
};
console.log("Sorted Data: \n", sortUsers());

//Q4 Find all users with masters Degree.

const usersWithMasterDegree = function () {
  let usersDoneMasters = Object.keys(users)
    .filter((user) => {
      return users[user].qualification === "Masters";
    })
    .reduce((newUserObj, user) => {
      newUserObj[user] = { ...users[user] };
      return newUserObj;
    }, {});
  return usersDoneMasters;
};
console.log("Users with Master Degree: \n ", usersWithMasterDegree());

//Q5 Group users based on their Programming language mentioned in their designation.

const groupWithDesignation = function () {
  let userGroupedWithDesignation = Object.keys(users).reduce(
    (groupedObj, user) => {
      let splitedDesignation = users[user].desgination.split(" ");
      let ProgrammingLanguage = "";
      if (splitedDesignation.includes("Senior")) {
        ProgrammingLanguage = splitedDesignation[1];
      } else if (splitedDesignation.includes("Intern")) {
        ProgrammingLanguage = splitedDesignation[2];
      } else {
        ProgrammingLanguage = splitedDesignation[0];
      }
      console.log();

      groupedObj[ProgrammingLanguage] ?? (groupedObj[ProgrammingLanguage] = []);

      let newobj = {};
      newobj[user] = {};
      newobj[user] = { ...users[user] };

      groupedObj[ProgrammingLanguage].push(newobj);
      return groupedObj;
    },
    {}
  );
  return userGroupedWithDesignation;
};
console.log("user grouped with designation : \n ", groupWithDesignation());
