/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/

const userUrl = "https://jsonplaceholder.typicode.com/users";
const todosUrl = "https://jsonplaceholder.typicode.com/todos";

//1. Fetch all the users
function fetchUsers() {
  return new Promise((resolve, reject) => {
    fetch(userUrl)
      .then((responce) => {
        return responce.json();
      })
      .then((userData) => {
        resolve(userData);
      })
      .catch((err) => {
        console.error(`Error in fetching user data`);
        reject(err);
      });
  });
}

// //2. Fetch all the todos data

function fetchTodos() {
  return new Promise((resolve, reject) => {
    fetch(todosUrl)
      .then((response) => {
        return response.json();
      })
      .then((todosData) => {
        resolve(todosData);
      })
      .catch((err) => {
        console.error(`Error in fetching todos data`);
        reject(err);
      });
  });
}

//3. Use the promise chain and fetch the users first and then the todos.

function fetchUserThenTodos() {
  return new Promise((resolve, reject) => {
    fetchUsers()
      .then((userData) => {
        //console.log(userData);
        return fetchTodos();
      })
      .then((todosData) => {
        //console.log(todosData);
        resolve();
      })
      .catch((err) => {
        console.error("Error in fetching data", err);
        reject(err);
      });
  });
}

// 4. Use the promise chain and fetch the users first and then all the details for each user.

function fetchUser(ID) {
  return new Promise((resolve, reject) => {
    if (!ID || typeof ID != "number") {
      reject("Invalid Id");
    } else {
      let eachUserUrl = `https://jsonplaceholder.typicode.com/users?id=${ID}`;
      fetch(eachUserUrl)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          console.error(`Error in getting user data`);
          reject(err);
        });
    }
  });
}
function fetchEachUser() {
  return new Promise((resolve, reject) => {
    fetchUsers()
      .then((userData) => {
        let promisesArray = userData.map((user) => {
          return fetchUser(user.id);
        });
        return Promise.all(promisesArray);
      })
      .then((usersData) => {
        resolve(usersData);
      })
      .catch((err) => {
        console.error("Error in fetching each user details");
        reject(err);
      });
  });
}

//5. Use the promise chain and fetch the first todo.
// Then find all the details for the user that is associated with that todo

function fetchTodoById(ID) {
  return new Promise((resolve, reject) => {
    if (!ID || typeof ID !== "number") {
      reject("Inproper ID");
    } else {
      fetch(`https://jsonplaceholder.typicode.com/todos?id=${ID}`)
        .then((responce) => {
          return responce.json();
        })
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          console.error("Error in fetching todos data", err);
          reject(err);
        });
    }
  });
}
function detailsOfuserforTodo() {
  return new Promise((resolve, reject) => {
    let id = 1;
    fetchTodoById(id)
      .then((todoData) => {
        return fetchUser(todoData[0].userId);
      })
      .then((userData) => {
        resolve(userData);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
}

fetchUsers()
  .then((userData) => {
    console.log("Users data fetched successfully");
    console.log(userData);
    return fetchTodos();
  })
  .then((todosData) => {
    console.log("Todos data fetched successfully ");
    console.log(todosData);
    return fetchUserThenTodos();
  })
  .then(() => {
    console.log("Fetching user data then todo data is successful");
    return fetchEachUser();
  })
  .then((usersData) => {
    console.log("Fetching details of each user is successful ");
    console.log(usersData);
    return detailsOfuserforTodo();
  })
  .then((userDetailsOfFirstTodo) => {
    console.log(
      "Details of user associated with first todo fetched successfully"
    );
    console.log(userDetailsOfFirstTodo);
  })
  .catch((err) => {
    console.error(err);
  });
