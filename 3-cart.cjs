const products = [
  {
    shampoo: {
      price: "$50",
      quantity: 4,
    },
    "Hair-oil": {
      price: "$40",
      quantity: 2,
      sealed: true,
    },
    comb: {
      price: "$12",
      quantity: 1,
    },
    utensils: [
      {
        spoons: { quantity: 2, price: "$8" },
      },
      {
        glasses: { quantity: 1, price: "$70", type: "fragile" },
      },
      {
        cooker: { quantity: 4, price: "$900" },
      },
    ],
    watch: {
      price: "$800",
      quantity: 1,
      type: "fragile",
    },
  },
];

/*
Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file
*/

//Q1. Find all the items with price more than $65.

const itemsWithMorePrice = function () {
  return products.map((itemBlock) => {
    return Object.entries(itemBlock).reduce(
      (itemsWithMorePrice, [item, information]) => {
        if (Array.isArray(information)) {
          let filteredArray = information.filter((insideItem) => {
            let key = Object.keys(insideItem);
            return insideItem[key].price.replace("$", "") > 65;
          });
          itemsWithMorePrice[item] = filteredArray;
        } else {
          if (information.price.replace("$", "") > 65) {
            itemsWithMorePrice[item] = { ...information };
          }
        }
        return itemsWithMorePrice;
      },
      {}
    );
  });
};
console.log(
  "Items with price more than 65 are : \n ",
  JSON.stringify(itemsWithMorePrice())
);

//Q2. Find all the items where quantity ordered is more than 1.
const itemsWithMoreQuantityOrdered = function () {
  return products.map((itemBlock) => {
    return Object.entries(itemBlock).reduce(
      (itemsWithMoreQuantity, [item, information]) => {
        if (Array.isArray(information)) {
          let filteredArray = information.filter((insideItem) => {
            let key = Object.keys(insideItem);
            return insideItem[key].quantity > 1;
          });
          itemsWithMoreQuantity[item] = filteredArray;
        } else {
          if (information.quantity > 1) {
            itemsWithMoreQuantity[item] = { ...information };
          }
        }
        return itemsWithMoreQuantity;
      },
      {}
    );
  });
};
console.log(
  "Items with quantity ordered more than 1 : \n ",
  itemsWithMoreQuantityOrdered()
);

//Q.3 Get all items which are mentioned as fragile.

const fragileItems = function () {
  return products.map((itemBlock) => {
    return Object.entries(itemBlock).reduce(
      (itemsWhichAreFragile, [item, information]) => {
        if (Array.isArray(information)) {
          let filteredArray = information.filter((insideItem) => {
            let key = Object.keys(insideItem);
            return insideItem[key].type === "fragile";
          });
          itemsWhichAreFragile[item] = filteredArray;
        } else {
          if (information.type === "fragile") {
            itemsWhichAreFragile[item] = { ...information };
          }
        }
        return itemsWhichAreFragile;
      },
      {}
    );
  });
};
console.log("Fragile Items are: \n ", fragileItems());

//Q.4 Find the least and the most expensive item for a single quantity.

const mostAndleastExpenciveItem = function () {
  let allItems = products.map((product) => {
    return Object.entries(product).reduce((allItemsArray, [item, details]) => {
      if (Array.isArray(details)) {
        allItemsArray.push(...details);
      } else {
        let newObj = {};
        newObj[item] = { ...details };

        allItemsArray.push(newObj);
      }
      return allItemsArray;
    }, []);
  });

  let sortedItems = allItems[0].sort((item1, item2) => {
    let item1Details = Object.values(item1)[0];
    let item2Details = Object.values(item2)[0];
    // Calculate amount for single quantity
    let item1Rate =
      parseInt(item1Details.price.replace("$", "")) / item1Details.quantity;
    let item2Rate =
      parseInt(item2Details.price.replace("$", "")) / item2Details.quantity;

    return item1Rate - item2Rate;
  });

  return [sortedItems[0], sortedItems[sortedItems.length - 1]];
};
console.log("Least and Most Expencive Item: \n ", mostAndleastExpenciveItem());

//Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

const groupItems = function () {
  return products.map((product) => {
    return Object.entries(product).reduce((groupedObj, [item, details]) => {
      groupedObj["Liquid"] ?? (groupedObj["Liquid"] = []);
      groupedObj["Solid"] ?? (groupedObj["Solid"] = []);
      groupedObj["Gas"] ?? (groupedObj["Gas"] = []);

      if (item === "shampoo" || item === "Hair-oil") {
        groupedObj["Liquid"].push({ item: { ...details } });
      } else if (item === "comb" || item === "watch" || item === "utensils") {
        groupedObj["Solid"].push({ item: { ...details } });
      }
      return groupedObj;
    }, {});
  });
};
console.log("Items grouped based on their state: \n ", groupItems());
